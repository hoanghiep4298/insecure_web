const express = require('express');
const bodyParser = require('body-parser');
const userRouter = require('./routes/users.route')
const loginRouter = require('./routes/login.route')
const registerRouter = require('./routes/register.route')
const pug = require('pug')
const db = require('./database/db_connection')

const app = express()
app.use(bodyParser.json())
app.set('view engine', 'pug')
app.use(express.static(__dirname + '/views'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(function(req, res, next) {
  
  res.setHeader("Access-Control-Allow-Credentials","true");
  res.setHeader("Access-Control-Allow-Headers","Content-Type");
next();
});

app.use('/user', userRouter);
app.use('/login', loginRouter);
app.use('/register', registerRouter)

app.listen(3000, function(){
	console.log('App is listening on port 3000')
})